
import os
import glob
import subprocess

H1_strain_files = sorted(glob.glob('/home/chris/strain_4k/H1/*'))
L1_strain_files = sorted(glob.glob('/home/chris/strain_4k/L1/*'))
n_files = len(H1_strain_files)

state = '/home/chris/bbhnet_inference/models/model_0.pt'
cmd_temp = 'python nn_inference.py --H1-file {} --L1-file {} --state {} --outfile {} --PSD-dir {}'
PSD_dir = '/home/chris/bbhnet_inference/PSD/00'

for i in range(n_files):
    H1_file = H1_strain_files[i]
    L1_file = L1_strain_files[i]

    GPS_string = os.path.splitext(os.path.basename(H1_file))[0].split('-')[2:]
    outfile = '/home/chris/bbhnet_inference/outdir/out-{}-{}.h5'.format(GPS_string[0], GPS_string[1])

    if not os.path.exists(outfile):
        cmd = cmd_temp.format(H1_file, L1_file, state, outfile, PSD_dir)
        print(i, cmd)
        subprocess.check_call(cmd.split(' '))

print('Done!')
